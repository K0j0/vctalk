﻿using UnityEngine;
using System.Collections;

public class ColorChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// this is a comment
		print("Color Changer ready");
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            GetComponent<MeshRenderer>().material.color = new Color(.5f, .5f, 1);
        }
	}
}
